\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Description of the ASIC Prototypes}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}LAUROC0}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}HLC1}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Test Bench}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Front-End Test Board}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Toy Calorimeter Board}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Front-End Mezzanine}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}DAQ Board}{9}{subsection.3.4}
\contentsline {section}{\numberline {4}Characterization of the Prototypes}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}LAUROC0}{10}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}input impedance tuning}{10}{subsubsection.4.1.1}
\contentsline {paragraph}{\nonumberline Setup}{10}{section*.9}
\contentsline {paragraph}{\nonumberline Method}{10}{section*.10}
\contentsline {paragraph}{\nonumberline Results}{10}{section*.11}
\contentsline {subsubsection}{\numberline {4.1.2}Linearity measurement}{10}{subsubsection.4.1.2}
\contentsline {paragraph}{\nonumberline Setup}{10}{section*.13}
\contentsline {paragraph}{\nonumberline Methods}{11}{section*.14}
\contentsline {paragraph}{\nonumberline Results}{11}{section*.15}
\contentsline {subsubsection}{\numberline {4.1.3}Characterization of the Internal Discriminator}{12}{subsubsection.4.1.3}
\contentsline {paragraph}{\nonumberline Setup}{12}{section*.18}
\contentsline {paragraph}{\nonumberline Method}{13}{section*.19}
\contentsline {paragraph}{\nonumberline Results}{13}{section*.20}
\contentsline {paragraph}{\nonumberline Conclusion}{14}{section*.21}
\contentsline {subsubsection}{\numberline {4.1.4}Noise Measurement}{14}{subsubsection.4.1.4}
\contentsline {paragraph}{\nonumberline Setup}{14}{section*.23}
\contentsline {paragraph}{\nonumberline Methods}{15}{section*.24}
\contentsline {paragraph}{\nonumberline Results}{15}{section*.25}
\contentsline {subsubsection}{\numberline {4.1.5}Cross-talk}{15}{subsubsection.4.1.5}
\contentsline {paragraph}{\nonumberline Setup}{15}{section*.27}
\contentsline {paragraph}{\nonumberline Method}{16}{section*.28}
\contentsline {paragraph}{\nonumberline Results}{16}{section*.29}
\contentsline {subsubsection}{\numberline {4.1.6}Behavior for Full LHC Orbits}{16}{subsubsection.4.1.6}
\contentsline {subsection}{\numberline {4.2}HLC1}{16}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}input impedance tuning}{16}{subsubsection.4.2.1}
\contentsline {paragraph}{\nonumberline Method}{16}{section*.31}
\contentsline {paragraph}{\nonumberline Results}{16}{section*.32}
\contentsline {subsubsection}{\numberline {4.2.2}Linearity measurement}{17}{subsubsection.4.2.2}
\contentsline {paragraph}{\nonumberline Method}{17}{section*.34}
\contentsline {paragraph}{\nonumberline Results}{17}{section*.35}
\contentsline {subsubsection}{\numberline {4.2.3}Noise Measurement}{17}{subsubsection.4.2.3}
\contentsline {paragraph}{\nonumberline Setup}{17}{section*.38}
\contentsline {paragraph}{\nonumberline Method}{18}{section*.39}
\contentsline {paragraph}{\nonumberline Results}{18}{section*.40}
\contentsline {subsubsection}{\numberline {4.2.4}Summing Outputs}{20}{subsubsection.4.2.4}
\contentsline {paragraph}{\nonumberline Setup}{20}{section*.43}
\contentsline {paragraph}{\nonumberline Methods}{20}{section*.44}
\contentsline {paragraph}{\nonumberline Results}{20}{section*.45}
\contentsline {subsubsection}{\numberline {4.2.5}Cross-talk}{21}{subsubsection.4.2.5}
\contentsline {paragraph}{\nonumberline Setup}{21}{section*.47}
\contentsline {paragraph}{\nonumberline Methods}{21}{section*.48}
\contentsline {paragraph}{\nonumberline Results}{21}{section*.49}
\contentsline {subsection}{\numberline {4.3}Summary and Comparison}{21}{subsection.4.3}
\contentsline {section}{\numberline {5}Conclusion}{21}{section.5}
\contentsline {part}{Appendices}{24}{part*.54}
